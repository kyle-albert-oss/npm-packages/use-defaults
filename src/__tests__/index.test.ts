import { AssertTrue, IsExact } from "conditional-type-checks";
import useDefaults from "../index";

interface ITest1 {
	a?: string,
	b: string,
	c?: string,
	d?: string | undefined,
	e: string | undefined,
}

describe("Basic spread approach", () => {
	it("Normal spread typing", () => {
		const inputOpts: ITest1 = { a: undefined, b: "b", e: "e" };
		const defaults: Partial<ITest1> = {
			a: "a",
			c: "c",
		};
		const opts = { ...defaults, ...inputOpts, test: "asdf" };

		type T1 = AssertTrue<IsExact<typeof opts, { a?: string | undefined, b: string, c?: string | undefined, d?: string | undefined, e: string | undefined, test: string }>>;

		expect(opts.c!.toUpperCase()).toEqual("C"); // ! assertion required in strict mode even though the value of 'c' is guaranteed to be "c" here
		expect(opts.a).toBeUndefined(); // key exists in input, so undefined is preserved.
		expect(opts.test).toEqual("asdf");
	});

	it("Normal spread typing - defaults is more specifically typed", () => {
		const inputOpts: ITest1 = { b: "b", c: undefined, e: "e" };
		const defaults: Required<Pick<ITest1, "a" | "c">> = {
			a: "a",
			c: "c",
		};
		const opts = { ...defaults, ...inputOpts, test: "asdf" };

		type T1 = AssertTrue<IsExact<typeof opts, { a: string, b: string, c: string, d?: string | undefined, e: string | undefined, test: string }>>;

		expect(opts.a).toEqual("a");
		expect(opts.c).toBeUndefined(); // Oops! The type of c is "c: string", but it's actually undefined here.
	});
});

describe("useDefaults", () => {
	describe("without enforced defaults", () => {
		it("works with property that has a default and is defined", () => {
			const _opts: ITest1 = { a: "test", b: "test2", e: "123" };
			const opts = useDefaults(_opts, {
				a: "A",
			});

			expect(opts.a).toEqual("test");
			expect(opts.b).toEqual("test2");
			expect(opts.c).toBeUndefined();
			expect(opts.d).toBeUndefined();
			expect(opts.e).toEqual("123");

			type T1 = AssertTrue<IsExact<typeof opts, ITest1>>;
		});

		it("works with property that has a default and is undefined", () => {
			const _opts: ITest1 = { b: "test2", e: undefined };
			const opts = useDefaults(_opts, {
				a: "A",
			});

			expect(opts.a).toEqual("A");
			expect(opts.b).toEqual("test2");
			expect(opts.c).toBeUndefined();
			expect(opts.d).toBeUndefined();
			expect(opts.e).toBeUndefined();

			type T1 = AssertTrue<IsExact<typeof opts, ITest1>>;
		});

		it("works with property that has a default and is explicitly undefined", () => {
			const _opts: ITest1 = { a: undefined, b: "test2", e: "123" };
			const opts = useDefaults(_opts, {
				a: "A",
			});

			expect(opts.a).toBeUndefined();
			expect(opts.b).toEqual("test2");
			expect(opts.c).toBeUndefined();
			expect(opts.d).toBeUndefined();
			expect(opts.e).toEqual("123");

			type T1 = AssertTrue<IsExact<typeof opts, ITest1>>;
		});
	});

	describe("with enforced defaults", () => {
		it("works with property that has an enforced default and is undefined", () => {
			const _opts: ITest1 = { b: "test2", e: undefined };
			const opts = useDefaults(_opts, {
				a: "A",
			}, ["a"]);

			expect(opts.a).toEqual("A");
			expect(opts.b).toEqual("test2");
			expect(opts.c).toBeUndefined();
			expect(opts.d).toBeUndefined();
			expect(opts.e).toBeUndefined();

			type T1 = AssertTrue<IsExact<typeof opts, { a: string, b: string, c?: string, d?: string, e: string | undefined }>>;
		});

		it("works with property that has an enforced default and is explicitly undefined", () => {
			const _opts: ITest1 = { a: undefined, b: "test2", e: "123" };
			const opts = useDefaults(_opts, {
				a: "A",
			}, ["a"]);

			expect(opts.a).toEqual("A");
			expect(opts.b).toEqual("test2");
			expect(opts.c).toBeUndefined();
			expect(opts.d).toBeUndefined();
			expect(opts.e).toEqual("123");

			type T1 = AssertTrue<IsExact<typeof opts, { a: string, b: string, c?: string, d?: string, e: string | undefined }>>;
		});

		it("works with multiple properties that have enforced defaults with one being undefined and the other defined.", () => {
			const _opts: ITest1 = { a: "ASDF", b: "test2", e: "123" };
			const opts = useDefaults(_opts, {
				a: "A",
				d: "CVB"
			}, ["a", "d"]);

			expect(opts.a).toEqual("ASDF");
			expect(opts.b).toEqual("test2");
			expect(opts.c).toBeUndefined();
			expect(opts.d).toEqual("CVB");
			expect(opts.e).toEqual("123");

			type T1 = AssertTrue<IsExact<typeof opts, { a: string, b: string, c?: string, d: string, e: string | undefined }>>;
		});

		it("works with mix of defaults that are enforced and not", () => {
			const _opts: ITest1 = { a: "ASDF", b: "test2", e: "123" };
			const opts = useDefaults(_opts, {
				a: "A",
				c: "5"
			}, ["a"]);

			expect(opts.a).toEqual("ASDF");
			expect(opts.b).toEqual("test2");
			expect(opts.c).toEqual("5");
			expect(opts.d).toBeUndefined();
			expect(opts.e).toEqual("123");

			type T1 = AssertTrue<IsExact<typeof opts, { a: string, b: string, c?: string, d?: string, e: string | undefined }>>;
		});

		it("works with all optional properties having enforced defaults", () => {
			const _opts: ITest1 = { a: "ASDF", b: "test2", e: "123" };
			const opts = useDefaults(_opts, {
				a: "A",
				c: "5",
				d: "zxc"
			}, ["a", "c", "d"]);

			expect(opts.a).toEqual("ASDF");
			expect(opts.b).toEqual("test2");
			expect(opts.c).toEqual("5");
			expect(opts.d).toEqual("zxc");
			expect(opts.e).toEqual("123");

			type T1 = AssertTrue<IsExact<typeof opts, { a: string, b: string, c: string, d: string, e: string | undefined }>>;
		});
	});
});
