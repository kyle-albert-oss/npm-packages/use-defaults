module.exports = {
	preset: "ts-jest",
	testEnvironment: "node",
	moduleFileExtensions: ["js", "ts"],
	modulePathIgnorePatterns: [
		"<rootDir>/dist/",
		"<rootDir>/out/"
	],
	transform: {
		"\\.(ts)$": "ts-jest"
	},
};
