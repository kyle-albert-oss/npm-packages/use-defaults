## 1.0.3
###### *Oct 30, 2019*

- README fixes.
- Restructure tests.

## 1.0.1
###### *Sep 28, 2019*

- Specify TypeScript min version (3.5) in README.
- Test grouping changes.
- Add coverage flag to test script.
- Meta updates to package.json.
- README fixes.
- CHANGELOG format changes.

## 1.0.0
###### *Sep 28, 2019*

- Initial release.
