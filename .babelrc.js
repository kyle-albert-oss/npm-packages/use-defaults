module.exports = {
	presets: [
		"@babel/preset-env",
		"@babel/typescript",
	],
	ignore: [/__tests__/, /\.test\..+$/],
};
